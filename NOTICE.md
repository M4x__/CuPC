 ------------------------------------------------------------------------------
| SPDX-FileCopyrightText: 2021 Maximilian Behm <CuPC@maxtheman.de>   |
| SPDX-License-Identifier: CERN-OHL-S-2.0+                                     |
|                                                                              |
| This source describes Open Hardware and is licensed under the CERN-OHL-S v2  |
| or any later version. 							  |
|                                                                              |
| You may redistribute and modify this source and make products using it under |
| the terms of the CERN-OHL-S v2 or any later version			  |
| (https://ohwr.org/cern_ohl_s_v2.txt).                                        |
|                                                                              |
| This source is distributed WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,          |
| INCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A         |
| PARTICULAR PURPOSE. Please see the CERN-OHL-S v2 for applicable conditions.  |
|                                                                              |
| Source location: https://gitlab.com/M4x__/CuPC                    |
|                                                                              |
| As per CERN-OHL-S v2 section 4, should You produce hardware based on this    |
| source, You must where practicable maintain the Source Location visible      |
| on the external case and the documentation of the learning tower or other    |
| products you make using this source.                                         |
|                                                                              |
| If you redistribute or modify this source or make products using it, I would |
| be very happy, if you would send me a note, providing the location of your   |
| published data and ideally a picture of the final product.                   |
|                                                                              |
| I followed the User Guide for the CERN Open Hardware Licence Version 2 -     |
| Strongly Reciprocal (September 2, 2020 / Guide version 1.0). Please have a   |
| look at this guide if you'd like to know more regarding the CERN-OHL         |
| license. It is a very good starting point and a great aid to                 |
| decision-making. The guide is part of the distributed source. It can also be |
| found here:                                                                  |
|   - PDF: https://ohwr.org/project/cernohl/wikis/uploads/                     |
|          cf37727497ca2b5295a7ab83a40fcf5a/cern_ohl_s_v2_user_guide.pdf       |
|   - TXT: https://ohwr.org/project/cernohl/wikis/uploads/                     |
|          b88fd806c337866bff655f2506f23d37/cern_ohl_s_v2_user_guide.txt       |
 ------------------------------------------------------------------------------
