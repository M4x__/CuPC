# CuPC - Customizable Pipe Cap

![exploded assembly (FreeCAD)]() <img src="pictures/animations/CuPC_exploded_assembly_rotation.gif" width=25% height=25%>`

A customizable, 3D-printable pipe cap. Adaptable to fit any pipe size. Easy to mount and dismount via a screw. Furthermore this can be used as a base to attach "stuff" to pipes.

It consists of two 3D-printable parts, a screw and a nut. The size of the screw and nut have to be chosen according to the user-defined parameters.

![customizable via a spreadsheet](pictures/screenshots/CuPC_fully_customizable_via_spreadsheet.png "spreadsheet for user-defined parameters")

![cut through assembled objects]() <img src="pictures/other/CuPC_assembled_object_cut_1-2_bgTransparent_cropped.png" width=50% height=50%>

Original design from takashi miyake (aka asimomagic): [Thingiverse 4795549](https://www.thingiverse.com/thing:4795549) (as of: 2021-08-30).

Unfortunately asimomagic used a proprietary CAD-software which isn't even available on the three major operation systems for desktop PCs (Linux, Mac OS and Windows). The files provided here are making it possible to use this project without a subscription, without an account and with either Linux, Mac OS or Windows. Enjoy your freedom - thanks to [FOSS (Free and Open Source Software)](https://itsfoss.com/what-is-foss/).

Designed in [FreeCAD](https://www.freecadweb.org/) v0.20 (last used version is 0.20.25943).

## Structure of the FreeCAD files
* **[`models/CuPC_components.FCStd`](models/CuPC_components.FCStd)**: All single components and the corresponding [TechDraw Pages](https://wiki.freecadweb.org/TechDraw_Module#Pages)
* **[`models/CuPC_assembly.FCStd`](models/CuPC_assembly.FCStd)**: The full assembly, the BOM , the corresponding [TechDraw Pages](https://wiki.freecadweb.org/TechDraw_Module#Pages) and copies of the cut through assembly and exploded assembly. The components are linked into this file using [App Link](https://wiki.freecadweb.org/App_Link)
* **[`models/CuPC_assembly_frozen_views.cam`](models/CuPC_assembly_frozen_views.cam)**: The [frozen views](https://wiki.freecadweb.org/Std_FreezeViews) used to create the [screenshots](pictures/screenshots/)

## Workbenches I've used (sorted alphabetically)
1. [Assembly3](https://wiki.freecadweb.org/Assembly3_Workbench)
2. [Part](https://wiki.freecadweb.org/Part_Module)
3. [PartDesign](https://wiki.freecadweb.org/PartDesign_Workbench)
4. [Spreadsheet](https://wiki.freecadweb.org/Spreadsheet_Workbench)
5. [TechDraw](https://wiki.freecadweb.org/TechDraw_Workbench)

## Macros I've used (sorted alphabetically)
1. [Macro EasyAlias](https://wiki.freecadweb.org/Macro_EasyAlias) (for Spreadsheets)
2. [Macro Screen Wiki](https://wiki.freecadweb.org/Macro_Screen_Wiki)
3. [Macro Snip](https://wiki.freecadweb.org/Macro_Snip)
4. [Macro TechDrawTools](https://wiki.freecadweb.org/Macro_TechDrawTools)
5. [Marco Vision color plus](https://forum.freecadweb.org/viewtopic.php?f=24&t=51952&p=538740#p538740)

## Things I'd do differently next time
Surprisingly: Nothing.

## Things I'd do the same next time
Regarding FreeCAD:
1. Use a [spreadsheet](https://wiki.freecadweb.org/Spreadsheet_Workbench) and [expressions](https://wiki.freecadweb.org/Expressions) to make calculations and keep the design as parametric as possible.
2. Use the [Assembly3 workbench](https://wiki.freecadweb.org/Assembly3_Workbench).
3. Export all drawings to PDF.
4. Export all components to STEP.
5. Make nice drawings using [TechDraw](https://wiki.freecadweb.org/TechDraw_Workbench) and think about when it's a good idea to reference [copies of bodies](https://wiki.freecadweb.org/Part_SimpleCopy) instead of the parametric (and therefore possibly changing) bodies when create drawings. :-)
6. Convert images (screenshots in this case) to [SVG](https://en.wikipedia.org/wiki/Scalable_Vector_Graphics) and use these files to place images in [TechDraw Pages](https://wiki.freecadweb.org/TechDraw_Module#Pages). [SVG](https://en.wikipedia.org/wiki/Scalable_Vector_Graphics)-files are getting "implemented" into the [FreeCAD](https://www.freecadweb.org/) file. This is not the case for [PNG](https://en.wikipedia.org/wiki/Portable_Network_Graphics)-files for example. For those, [TechDraw](https://wiki.freecadweb.org/TechDraw_Workbench) uses the **absolute** path to the [PNG](https://en.wikipedia.org/wiki/Portable_Network_Graphics)-file.

## Possible further developments
I've no ideas for further improvements at the moment.

## Fuckups
This time: None! Who would've thought?

## Other thoughts / general information
Usually I'm giving the advice to center all models around the center of the coordination system and / or position models in a way which makes it easy to make use of the axis of the coordination system regarding mirroring a part. This time I didn't do all of it.

Instead, I've positioned the parts in relationship to each other like they're already assambled. And I've used one axis to make the modeling of the flange easier (one axis lays between the flange and the rest of the body). I did this because the parts aren't that easy to assemble within an assembly workbench (because of there shape), it made it easier for me to check if everything "fits" and the assembly is technically pretty straight forward.

## Thank you!
Projects like this wouldn't be possible or a lot harder to realize (and document) if the great software I've used so far wouldn't exist / be available. All of it is open source! Thank you very much (alphabetic order):
- [FreeCAD](https://www.freecadweb.org/)
- [GIMP](https://www.gimp.org/)
- [Inkscape](https://inkscape.org/)
- [peek](https://github.com/phw/peek)
- [Ubuntu](https://ubuntu.com/)

I hope that I can give something back to the community by sharing my files / this project.

## License
SPDX-FileCopyrightText: 2021 Maximilian Behm <CuPC@maxtheman.de>
SPDX-License-Identifier: CERN-OHL-S-2.0+

This source describes Open Hardware and is licensed under the CERN-OHL-S v2
or any later version.

You may redistribute and modify this source and make products using it under
the terms of the CERN-OHL-S v2 or any later version
(https://ohwr.org/cern_ohl_s_v2.txt).

This source is distributed WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,
INCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A
PARTICULAR PURPOSE. Please see the CERN-OHL-S v2 for applicable conditions.

Source location: https://gitlab.com/M4x__/CuPC

As per CERN-OHL-S v2 section 4, should You produce hardware based on this
source, You must where practicable maintain the Source Location visible
on the external case and the documentation of the learning tower or other
products you make using this source.

If you redistribute or modify this source or make products using it, I would 
be very happy, if you would send me a note, providing the location of your
published data and ideally a picture of the final product.

I followed the User Guide for the CERN Open Hardware Licence Version 2 -
Strongly Reciprocal (September 2, 2020 / Guide version 1.0). Please have a
look at this guide if you'd like to know more regarding the CERN-OHL
license. It is a very good starting point and a great aid to
decision-making. The guide is part of the distributed source. It can also be
found here:
  - PDF: https://ohwr.org/project/cernohl/wikis/uploads/cf37727497ca2b5295a7ab83a40fcf5a/cern_ohl_s_v2_user_guide.pdf
  - TXT: https://ohwr.org/project/cernohl/wikis/uploads/b88fd806c337866bff655f2506f23d37/cern_ohl_s_v2_user_guide.txt

